package com.rapid.servicea.hystrix;

import com.rapid.servicea.service.HelloService;
import org.springframework.stereotype.Component;

@Component
public class HelloServiceHystrix implements HelloService {

    @Override
    public String hi() {
        return "hi ,this is hystrix!";
    }
}