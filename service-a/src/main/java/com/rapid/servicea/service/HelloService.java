package com.rapid.servicea.service;


import com.rapid.servicea.hystrix.HelloServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "service-b",fallback = HelloServiceHystrix.class)
public interface HelloService {

    @RequestMapping("/hi")
    public String hi();
}