package com.rapid.servicea.controller;

import com.rapid.servicea.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RefreshScope
public class ConfigClientController {

    @Value("${name}")
    String name;
    @Value("${age}")
    String age;
    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello")
    public String hello() {
        return "name: "+name+",age: "+age;
    }

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @GetMapping("/hi")
    public String hi(){
        return restTemplate().getForEntity("http://localhost:8002/hi",String.class).getBody();
    }

    @GetMapping("/gethi")
    public String gethi(){
        return helloService.hi();

    }
}