package com.rapid.serviceb.service;


import com.rapid.serviceb.hystrix.HelloServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "service-a",fallback = HelloServiceHystrix.class)
public interface HelloService {

    @RequestMapping("/hello")
    public String hello();
}