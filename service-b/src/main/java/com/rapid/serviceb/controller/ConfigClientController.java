package com.rapid.serviceb.controller;

import com.rapid.serviceb.service.HelloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigClientController {

    @Value("${name}")
    String name;
    @Value("${age}")
    String age;
    @Autowired
    private HelloService helloService;

    @RequestMapping("/hello")
    public String hello() {
        return "name: "+name+",age: "+age;
    }

    @GetMapping("/hi")
    public String hi(){
        return "hello kitty!";
    }

    @RequestMapping("/gethello")
    public String gethello() {
        return helloService.hello();
    }
}