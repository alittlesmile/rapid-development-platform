package com.rapid.serviceb.hystrix;

import com.rapid.serviceb.service.HelloService;
import org.springframework.stereotype.Component;

@Component
public class HelloServiceHystrix implements HelloService {

    @Override
    public String hello() {
        return "hello ,this is hystrix!";
    }
}